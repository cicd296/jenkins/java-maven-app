def incrementVersion() {
    echo 'incrementing app version...'
    sh 'mvn build-helper:parse-version versions:set \
        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
        versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"  
}

def buildJar() {
	echo "building the application..."
    sh "mvn clean package"
}

def buildImage() {
    echo 'building the docker image...'
    withCredentials([usernamePassword(credentialsId: 'nexus-docker-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        //sh "docker build -t 192.168.100.119:8083/java-maven-app:2.0 ."
		sh "docker build -t ${params.DOCKER_REPO}/${params.DOCKER_APP_NAME}:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin ${params.DOCKER_REPO}"
        sh "docker push ${params.DOCKER_REPO}/${params.DOCKER_APP_NAME}:${IMAGE_NAME}"
    }
}

def deployApp() {
	echo "deploying the application..."
}

def commitVersionUpdate() {
	withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "git config --global user.email 'jenkins@example.com'" //--global for all projects
        sh "git config --global user.name 'jenkins'"

        //sh "git status"
        //sh "git branch"
        //sh "git config --list"

        //issues with password with special chars: https://fabianlee.org/2016/09/07/git-calling-git-clone-using-password-with-special-character/
        sh "git remote set-url origin https://$USER:$PASS@gitlab.com/clmbv/java-maven-app.git"
        sh "git add ."
        sh "git commit -m 'ci version bump'"
        sh "git push origin HEAD:master"
    }
}

return this
